package org.hablapps.hofs
package templates
package forloopsremoval
package withviews

object IteratorsAdaptedSolution extends IteratorsAdaptedSolution
class IteratorsAdaptedSolution extends withloops.ProblemVariation {

  /**
   * Solution: HOFs version
   */

  def lengths(list: List[String]): List[Int] =
    ???
    // list.flatMap(_.split(" "))
    //   .filter(_ != "")
    //   .map(_.length)

  /**
   * Test solution
   */

  ignore("5- Problem variation: iterator version"){
    test(lengths)
  }
}
