package org.hablapps.fpinscala.hofs
package templates

import org.scalatest._

// 8. Catamorphisms are usually higher-order functions that allow us
//    to interpret any ADT in a compositional way (taking into account
//    its structure and the meaning or interpretations of their parts).

class Catamorphism extends FunSpec with Matchers{

  // Catamorphism for `Either`

  def fold[A,B,C](e: Either[A,B])(left: A => C, right: B => C): C = ???

  describe("fold for Either"){
    ignore("should work"){
      fold(Left[String,Int](""))(_.length, identity) shouldBe (??? : Int)
      fold(Right[String,Int](3))(_.length, identity) shouldBe (??? : Int)
    }
  }

  // Catamorphism for `List`

  def fold[A, B](l: List[A])
      (nil: B, cons: (A, B) => B): B = ???

  describe("fold for List"){
    ignore("should work"){
      fold(Nil: List[String])(0,(s: String, i: Int) => s.length + i) shouldBe (??? : Int)
      fold(List("","a","b"))(0,(s: String, i: Int) => s.length + i) shouldBe (??? : Int)
    }
  }

  // Lots of functions can be defined in terms of catamorphisms,
  // e.g. adding integers

  def sum(l: List[Int]): Int = ???

}

