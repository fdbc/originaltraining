package org.hablapps.fpinscala.hofs
package diagrams
package templates

object DrawingRepr{
  import PictureDSL._

  // Auxiliary type

  case class Pos(x: Double, y: Double) {
    def +(other: Pos): Pos = Pos(x+other.x, y+other.y)
  }

  /**
   * Drawings
   */

  type Drawing = Nothing

  object Drawing{

    type Component = Nothing

    // Drawing constructor: picture interpreter

    def apply(p: Picture): Drawing = ???

    
    // Lower left and upper right corners

    type Extent = (Pos, Pos)

    def extent(drawing: Drawing): Extent = ???

  }
}