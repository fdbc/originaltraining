package org.hablapps.fpinscala.hofs
package diagrams
package templates

object PictureDSL{

  /**
   * SHAPE ADT
   */

  // Shape = Rectangle + Circle + Triangle
  //       = Double * Double + Double + Double

  sealed abstract class Shape

  object Shape{

  }

  /**
   * STYLE SHEETS
   */

  type StyleSheet = Nothing

  // Styling = FillColor + StrokeColor + StrokeWidth
  //         = Color + Color + Double

  sealed abstract class Styling

  object Styling{

  }

  // Color = Red + Blue + Green + ....
  //       = 1 + 1 + 1 + ....

  sealed abstract class Color

  /**
   * Pictures
   */

  // Picture = Place + Above + Beside
  //         = StyleSheet * Shape + Picture * Picture + Picture * Picture

  sealed abstract class Picture

  object Picture{


    // Syntax

    object Syntax{

    }
  }
}
