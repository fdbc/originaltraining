package org.hablapps.fpinscala.hofs
package diagrams
package code

object SVG {
  import PictureDSL._, DrawingRepr._

  // Picture interpreter

  def apply(picture: Picture): String =
    apply(Drawing(picture))
  
  // Drawing interpreter

  def apply(drawing: Drawing): String = {
    // HEADER
    val (width, height) = size(drawing)
    val header =
      s"""|<svg width="$width" height="$height" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
          |  <g transform="translate(${width/2}, ${height/2})">
          |    <g transform="scale(1, -1)">""".stripMargin + "\n      "

    // SHAPES
    val draw = drawing.map{
      case (t, ss, s) => shapeToSVG(s)(t)(ss)
    }.mkString("\n      ") + "\n"

    // FOOTER
    val footer =
      s"""|    </g>
          |  </g>
          |</svg>""".stripMargin

    // ALL TOGETHER
    header ++ draw ++ footer
  }

  // Size of drawing

  def size(d: Drawing): (Double, Double) = Drawing.extent(d) match {
    case (Pos(llx, lly), Pos(urx, ury)) => (urx - llx, ury - lly)
  }

  // Style interpreter

  def styleToSVG(style: StyleSheet): String =
    style.map(stylingToSVG).mkString(" ")

  def stylingToSVG(styling: Styling): String =
    Styling.fold(styling)(fillColorToSVG, _ => ???, _ => ???)

  def fillColorToSVG(color: Color): String =
    s"""fill=${colorToSVG(color)}"""

  def colorToSVG: Color => String = {
    case Bisque => "#000"
    case Red => "#f00"
    case Blue => "#00f"
    case Alpha => "none"
    case _ => "#000"
  }

  // Shape interpreter

  def shapeToSVG(s: Shape): Pos => StyleSheet => String =
    Shape.fold(s)(rectangleToSVG, circleToSVG, triangleToSVG)

  def circleToSVG(radius: Double)(t: Pos)(ss: StyleSheet): String = {
    val Pos(x, y) = t + Pos(0, 0) // t
    s"""<circle r="$radius" cx="$x" cy="$y" ${styleToSVG(ss)} />"""
  }

  def rectangleToSVG(width: Double, height: Double)(t: Pos)(ss: StyleSheet): String = {
    val Pos(x, y) = t + Pos(-width/2, -height/2)
    s"""<rect x="$x" y="$y" width="$width" height="$height" ${styleToSVG(ss)} />"""
  }

  def triangleToSVG(width: Double)(t: Pos)(ss: StyleSheet): String = {
    val height = math.sqrt(3) * width / 2
    val Pos(x, y) = t + Pos(-width/2, -height/2)
    s"""<polygon points="$x,$y ${x+(width/2)},${y+height} ${x+width},$y" ${styleToSVG(ss)} />"""
  }
}