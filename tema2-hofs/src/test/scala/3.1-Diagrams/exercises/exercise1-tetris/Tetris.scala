// package org.hablapps.fpinscala.hofs

// // Exercise: Implement tetris figures using the Diagrams library

// // you can test this exercise using the alias `test-hofs-tetris`
// object Tetris {
//   import diagrams.code._, Picture._

//   // This picture represents a 1x1 square of given color. Use it
//   // as a building block to create other figures.
//   def block(c: Color): Picture =
//     Place(FillColor(c) :: Nil, Rectangle(100, 100))

//   val I: Picture =
//     block(Maroon) above
//     block(Maroon) above
//     block(Maroon) above
//     block(Maroon)

//   val J: Picture =
//     (block(Alpha)     beside block(LightGray)) above
//     (block(Alpha)     beside block(LightGray)) above
//     (block(LightGray) beside block(LightGray))

//   // Use `Purple` color for this figure
//   val L: Picture = ???

//   // Use `NavyBlue` color for this figure
//   val O: Picture = ???

//   // Use `DarkGreen` color for this figure
//   val S: Picture = ???

//   // Use `Brown` color for this figure
//   val T: Picture = ???

//   // Use `Teal` color for this figure
//   val Z: Picture = ???

//   def show() = {
//     println("L:")
//     println(SVG.toSVG(Drawing(L)))
//     println("O:")
//     println(SVG.toSVG(Drawing(O)))
//     println("S:")
//     println(SVG.toSVG(Drawing(S)))
//     println("T:")
//     println(SVG.toSVG(Drawing(T)))
//     println("Z:")
//     println(SVG.toSVG(Drawing(Z)))
//   }

//   // Uncomment the following line to check the SVG results
//   // show

// }