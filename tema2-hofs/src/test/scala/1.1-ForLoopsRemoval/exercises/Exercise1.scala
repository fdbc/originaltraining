package org.hablapps.hofs
package forloopsremoval
package exercises

class Exercise1 extends Exercise1Spec{

  /**
   * PART I.
   *
   * Use the `foldLeft` [1] function to implement the `sumEvenNumbers` function.
   *
   * [1] http://www.scala-lang.org/api/2.12.3/scala/collection/immutable/List.html#foldLeft[B](z:B)(op:(B,A)=>B):B
   *
   */
  object WithHOFWithoutFilter{

    def sumEvenNumbers(list: List[Int]): Int = ???
  }

  ignore("HOF program without filter"){
    test(WithHOFWithoutFilter.sumEvenNumbers)
  }

  /**
   * PART II.
   *
   * Use the `foldLeft` and the `filter` function [1].
   *
   * [1] http://www.scala-lang.org/api/2.12.3/scala/collection/immutable/List.html#filter(p:A=>Boolean):Repr
   */
  object WithHOFWithFilter{

    def sumEvenNumbers(list: List[Int]): Int = ???
  }

  ignore("HOF program with filter"){
    test(WithHOFWithFilter.sumEvenNumbers)
  }

}