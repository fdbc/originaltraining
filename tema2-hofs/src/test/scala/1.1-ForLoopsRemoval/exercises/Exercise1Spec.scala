package org.hablapps.hofs
package forloopsremoval
package exercises

import org.scalatest._

abstract class Exercise1Spec extends FunSpec with Matchers{

  def test(sumEvenNumbers: List[Int] => Int){
    it("should work for empty lists"){
      sumEvenNumbers(List()) shouldBe 0
    }

    it("should work for lists of only even numbers"){
      sumEvenNumbers(List(2,4,6,8)) shouldBe 20
    }

    it("should work for lists of only odd numbers"){
      sumEvenNumbers(List(1,3,5,7)) shouldBe 0
    }
  }

}
