package org.hablapps.hofs
package forloopsremoval
package withhofs

object HOFsAdaptedSolution extends HOFsAdaptedSolution
class HOFsAdaptedSolution extends withloops.ProblemVariation {

  /**
   * Solution: HOFs version
   */

  def lengths(list: List[String]): List[Int] =
    list.flatMap(_.split(" "))
      .filter(_ != "")
      .map(_.length)

  /**
   * Test solution
   */

  describe("Problem variation: HOFs version"){
    test(lengths)
  }
}
