package org.hablapps.fpinscala.hofs.homework

// Test this exercise using the sbt alias `test-hofs-hw1`
object Homework1 {

  object PartI {
    // Implement `filter` for `Option`
    def filter[A](o: Option[A])(p: A => Boolean): Option[A] = ???

    // Implement `map` for `Option`
    def map[A, B](o: Option[A])(f: A => B): Option[B] = o match {
      case None => ???
      case Some(x) => ???
    }
  }

  object PartII {
    // Implement `fold` for `Option`
    def fold[A, B](o: Option[A])(b: B)(f: A => B): B = ???

    // Implement `filter` as `fold`
    def filter[A](o: Option[A])(p: A => Boolean): Option[A] =
      fold(???)(???)(???)

    // Implement `map` as `fold`
    def map[A, B](o: Option[A])(f: A => B): Option[B] =
      fold(???)(???)(???)
  }
}