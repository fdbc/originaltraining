package org.hablapps.fpinscala.hofs
package test

import org.scalatest._
import homework.Homework2._

// Test this exercise with sbt alias `test-hofs-hw2`

class Hw2Spec extends FlatSpec with Matchers {

  val t1 = Node(Node(Node(Node(Empty(), 5, Empty()), 4, Empty()), 2, Empty()), 1, Node(Empty(), 3, Empty()))
  val emptyT = Empty[Int]()
  def foldFunction(l: Int, i: Int, r: Int): Int = (l :: i :: r :: Nil).max

  "Sum" should "wotk for binary trees" in {
    PartII.sum(t1) shouldBe 15
  }

  "Max" should "wotk for binary trees" in {
    PartII.max(t1) shouldBe 5
  }

  "Empties" should "wotk for binary trees" in {
    PartII.emptyTrees(t1) shouldBe 6
  }

  "Depth" should "wotk for binary trees" in {
    PartII.depth(t1) shouldBe 4
  }

  "Fold" should "wotk for binary trees" in {
    PartIII.fold(emptyT)(1, foldFunction) shouldBe 1
    PartIII.fold(t1)(0, foldFunction) shouldBe 5
  }

  "SumFold" should "wotk for binary trees" in {
    PartIII.sum(t1) shouldBe 15
  }

  "MaxFold" should "wotk for binary trees" in {
    PartIII.max(t1) shouldBe 5
  }

  "EmptiesFold" should "wotk for binary trees" in {
    PartIII.emptyTrees(t1) shouldBe 6
  }

  "DepthFold" should "wotk for binary trees" in {
    PartIII.depth(t1) shouldBe 4
  }

}
