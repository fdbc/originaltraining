package org.hablapps.fpinscala.hofs

import org.scalatest._

class HallOfFame extends FunSpec with Matchers {

  // Map

  def map[A,B](v: Option[A])(f: A => B): Option[B] =
    v match {
      case None => None
      case Some(a) => Some(f(a))
    }

  def map[A,B](v: List[A])(f: A => B): List[B] =
    v match {
      case Nil => Nil
      case head :: tail => f(head) :: map(tail)(f)
    }

  describe("Map functions") {

    it("should work for Option"){
      map(Some(1))(_ + 1) shouldBe Some(2)
      map(Option.empty[Int])(_ + 1) shouldBe None
    }

    it("should work for List"){
      map(Nil: List[Int])(_ + 1) shouldBe Nil
      map(1 :: 2 :: Nil)(_ + 1) shouldBe 2 :: 3 :: Nil
    }
  }

  // Filter

  def filter[A](v: Option[A])(f: A => Boolean): Option[A] =
    v match {
      case Some(a) if f(a) => Some(a)
      case _ => None
    }

  def filter[A](v: List[A])(f: A => Boolean): List[A] =
    v match {
      case Nil => Nil
      case head :: tail if f(head) => head :: filter(tail)(f)
      case _ :: tail => filter(tail)(f)
    }

  describe("Filter functions") {

    it("should work for Option"){
      filter(Some(1))(_ % 2 == 0) shouldBe None
      filter(Some(2))(_ % 2 == 0) shouldBe Some(2)
      filter(Option.empty[Int])(_ % 2 == 0) shouldBe None
    }

    it("should work for List"){
      filter(Nil: List[Int])(_ % 2 == 0) shouldBe Nil
      filter(1 :: 2 :: 3 :: 4 :: Nil)(_ % 2 == 0) shouldBe 2 :: 4 :: Nil
    }

  }

  // FlatMap

  def flatMap[A,B](v: Option[A])(f: A => Option[B]): Option[B] =
    v match {
      case None => None
      case Some(a) => f(a)
    }

  def flatMap[A,B](v: List[A])(f: A => List[B]): List[B] =
    v match {
      case Nil => Nil
      case head :: tail => f(head) ::: flatMap(tail)(f)
    }

  describe("FlatMap functions") {

    it("should work for Option"){
      def add(i: Int): Int => Option[Int] =
        j => Some(i+j)

      flatMap(Some(1))(add(5)) shouldBe Some(6)
      flatMap(Option.empty[Int])(add(1)) shouldBe None
    }

    it("should work for List"){
      def span(i: Int): List[Int] =
        List(i, -i)

      flatMap(Nil: List[Int])(span) shouldBe Nil
      flatMap(1 :: 2 :: Nil)(span) shouldBe 1 :: -1 :: 2 :: -2 :: Nil
    }
  }


}
