package org.hablapps.fpinscala.hofs
package solution

import org.scalatest._

/**
 * Implement the following HOFs that allow us to create functions
 * compositionally.
 */
class CompositionSpec extends FunSpec with Matchers {

  // Implement `compose`

  def compose[A,B,C](g: B => C, f: A => B): A => C = a => g(f(a))

  // Implement `andThen` using `compose`

  def andThen[A,B,C](f: A => B, g: B => C): A => C = compose(g, f)

  // Test

  describe("Composition"){
    def square(i:Int) = i * i
    def toQuotedString(i:Int) = "'" + i.toString + "'"

    it("should work"){
      compose(toQuotedString, square)(10) shouldBe "'100'"
      andThen(square, toQuotedString)(10) shouldBe "'100'"
    }
  }

}
