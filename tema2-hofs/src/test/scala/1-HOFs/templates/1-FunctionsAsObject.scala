package org.hablapps.fpinscala.hofs
package templates

// 6. Functions as objects in Scala
object Objects {

  // 6.1. Functions defined in `Methods.scala`
  def length(s: String): Int = s.length
  def add(i: Int, j: Int): Int = i + j
  def times(i: Int, j: Int): Int = i * j
  def odd(i: Int): Boolean = i % 2 == 1
  def even(i: Int): Boolean = !odd(i)
  def five: Int = 5

  // 6.2. Same functions using objects (and lambda expressions)
  val lengthObj: String => Int = ???
  val addObj: (Int, Int) => Int = ???
  val timesObj: (Int, Int) => Int = ???
  val oddObj: Int => Boolean = ???
  val evenObj: Int => Boolean = ???
  val fiveObj: Unit => Int = ???

  // 6.3. From "method" functions to "object" functions through `eta expansion`
  val lengthExp = ???
  val addExp = ???
  val timesExp = ???
  val oddExp = ???
  val evenExp = ???
  val fiveExp = ???
}
