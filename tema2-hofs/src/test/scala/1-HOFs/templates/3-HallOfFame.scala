package org.hablapps.fpinscala.hofs
package templates

import org.scalatest._

class HallOfFame extends FunSpec with Matchers {

  // Map

  def map[A,B](v: Option[A])(f: A => B): Option[B] = ???

  def map[A,B](v: List[A])(f: A => B): List[B] = ???

  describe("Map functions") {

    ignore("should work for Option"){
      map(Some(1))(_ + 1) shouldBe ???
      map(Option.empty[Int])(_ + 1) shouldBe ???
    }

    ignore("should work for List"){
      map(Nil: List[Int])(_ + 1) shouldBe ???
      map(1 :: 2 :: Nil)(_ + 1) shouldBe ???
    }
  }

  // Filter

  def filter[A](v: Option[A])(f: A => Boolean): Option[A] = ???

  def filter[A](v: List[A])(f: A => Boolean): List[A] = ???

  describe("Filter functions") {

    ignore("should work for Option"){
      filter(Some(1))(_ % 2 == 0) shouldBe ???
      filter(Some(2))(_ % 2 == 0) shouldBe ???
      filter(Option.empty[Int])(_ % 2 == 0) shouldBe ???
    }

    ignore("should work for List"){
      filter(Nil: List[Int])(_ % 2 == 0) shouldBe ???
      filter(1 :: 2 :: 3 :: 4 :: Nil)(_ % 2 == 0) shouldBe ???
    }

  }

  // FlatMap

  def flatMap[A,B](v: Option[A])(f: A => Option[B]): Option[B] = ???

  def flatMap[A,B](v: List[A])(f: A => List[B]): List[B] = ???

  describe("FlatMap functions") {

    ignore("should work for Option"){
      def add(i: Int): Int => Option[Int] =
        j => Some(i+j)

      flatMap(Some(1))(add(5)) shouldBe ???
      flatMap(Option.empty[Int])(add(1)) shouldBe ???
    }

    ignore("should work for List"){
      def span(i: Int): List[Int] =
        List(i, -i)

      flatMap(Nil: List[Int])(span) shouldBe ???
      flatMap(1 :: 2 :: Nil)(span) shouldBe ???
    }
  }

}
