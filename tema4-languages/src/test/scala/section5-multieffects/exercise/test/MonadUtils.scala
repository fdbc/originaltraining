package org.hablapps.fpinscala
package languages
package test

import org.scalatest._

import scalaz.{State, StateT, MonadState, Monad => MonadZ}

// Monad utils
object MonadUtils{

  implicit def scalazMonadMonad[M[_]: MonadZ] = new MonadicPrograms.Monad[M] {
    def doAndThen[A, B](ma: M[A])(f: A => M[B]): M[B] =
      MonadZ[M].bind(ma)(f)

    def returns[A](value: A): M[A] =
      MonadZ[M].point(value)
  }

}