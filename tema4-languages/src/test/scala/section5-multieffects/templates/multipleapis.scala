package org.hablapps.fpinscala
package languages
package templates

import org.scalatest._

object MultipleAPIs extends MultipleAPIs

class MultipleAPIs extends FlatSpec with Matchers{

  // API

  // trait Log{
  //   def log(level: Log.Level, msg: String): Unit
  //   def warn(msg: String): Unit = log(Log.WARN,msg)
  //   def debug(msg: String): Unit = log(Log.DEBUG,msg)
  //   def trace(msg: String): Unit = log(Log.TRACE,msg)
  // }

  // object Log{
  //   sealed abstract class Level
  //   case object WARN extends Level
  //   case object DEBUG extends Level
  //   case object TRACE extends Level
  // }

  // Api instance

  // object ConsoleLog extends Log{
  //   def log(level: Log.Level, msg: String) =
  //     println(s"$level: $msg")
  // }

  // Api program

  // import Modular.IO

  // def echoWithLog()(io: IO, log: Log): String = {
  //   val msg: String = io.read()
  //   log.trace(s"read $msg")
  //   io.write(msg)
  //   log.trace(s"written $msg")
  //   msg
  // }

  // Composition

  // import Modular.ConsoleIO
  // def consoleEchoWithLog(): String =
  //   echoWithLog()(ConsoleIO, ConsoleLog)
}