package org.hablapps.fpinscala.languages
package test

import org.scalatest._
import Homework1._
import scalaz.State

class Homework1Spec extends FlatSpec with Matchers {

  import FSS._
  import MonadUtils._

  "Move" should "mover un fichero de un path a otro" in {
    val fs = FSS(Map("file1" -> "contenido file1"))
    move[FSState]("file1", "file2").exec(fs) shouldBe
      FSS(Map("file2" -> "contenido file1"))
  }

  "Merge2" should "juntar el contenido de dos ficheros en un tercero" in {
    val fs = FSS(Map(
      "file1" -> "contenido file1",
      "file2" -> "contenido file2"))
    merge2[FSState]("file1", "file2", "file3").exec(fs) shouldBe
      FSS(Map(
        "file1" -> "contenido file1",
        "file2" -> "contenido file2",
        "file3" -> "contenido file1contenido file2"))
  }

  "Merge2Bis" should "juntar el contenido de dos ficheros en un tercero" in {
    val fs = FSS(Map(
      "file1" -> "contenido file1",
      "file2" -> "contenido file2"))
    merge2bis[FSState]("file1", "file2", "file3").run(fs) shouldBe
      (FSS(Map(
        "file1" -> "contenido file1",
        "file2" -> "contenido file2",
        "file3" -> "contenido file1contenido file2")),
      30)
  }

  "MergeN" should "juntar el contenido de N ficheros en un destino" in {
    val fs = FSS(Map(
      "file1" -> "contenido file1",
      "file2" -> "contenido file2",
      "file3" -> "contenido file3"))
    mergeN[FSState]("file1" :: "file2" :: "file3" :: Nil, "dest").exec(fs) shouldBe FSS(Map(
      "file1" -> "contenido file1",
      "file2" -> "contenido file2",
      "file3" -> "contenido file3",
      "dest" -> "contenido file1contenido file2contenido file3"))
  }

  it should "funcionar con un solo fichero de entrada" in {
    val fs = FSS(Map(
      "file1" -> "contenido file1"))
    mergeN[FSState](List("file1"), "dest").exec(fs) shouldBe FSS(Map(
      "file1" -> "contenido file1",
      "dest" -> "contenido file1"))
  }

  it should "funcionar sin ningún fichero de entrada" in {
    val fs = new FSS(Map())
    mergeN[FSState](List(), "dest").exec(fs) shouldBe
      FSS(Map("dest" -> ""))
  }

}
