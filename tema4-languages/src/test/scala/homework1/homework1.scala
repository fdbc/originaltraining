package org.hablapps.fpinscala
package languages

object Homework1{
  import MonadicPrograms.Monad
  import FS.Syntax._, Monad.Syntax._

  /**
   * Part I.
   *
   * Implement a program that moves a file from one place to another.
   */
  import Exercise2Program1.Declarative.copy

  def move[F[_]: FS: Monad](orig: String, dest: String): F[Unit] = ???

  /**
   * Part II.
   *
   * Implement a program that merges together the content of two files.
   */
  def merge2[F[_]: FS: Monad](orig1: String, orig2: String, dest: String): F[Unit] = ???

  /**
   * Part III.
   *
   * Implement a program that does the same as the above, but also returns
   * the whole length of the destination file.
   */
  def merge2bis[F[_]: FS: Monad](orig1: String, orig2: String, dest: String): F[Int] = ???

  /**
   * Part IV.
   *
   * Implement a program similar to `merge2`, but fo N files.
   */
  def mergeN[F[_]: FS: Monad](origs: Seq[String], dest: String): F[Unit] = ???

}