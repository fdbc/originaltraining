package org.hablapps.fpinscala
package languages
package test

import org.scalatest._
import Homework2._
import MonadUtils._

class Homework2Spec extends FlatSpec with Matchers {

  import TerminalState.TState, FSS._, IOState._

  val terminal = Terminal[TState]

  "Help" should "show a help message" in {
    val ts = TerminalState(
      io = IOState(Nil, Nil),
      fs = FSS())

    terminal.help.exec(ts) shouldBe TerminalState(
      io = IOState(Nil, """ |List of commands available:
                            |- help: show this help message
                            |- cp <orig> <dest>: copy a file from <orig> to <dest>
                            |- mv <orig> <dest>: move a file from <orig> to <dest>
                            |- rm <file>: remove <file> from the filesystem
                            |- touch <file>: creates a file with empty content
                            |- touch2 <file> [contents]: creates a file with [contents]
                            |- cat <file>: shows the contents of <file>
                            |- echo [contents]: prints [contents] to the stdout
                            |- exit: close terminal
                            |""".stripMargin :: Nil),
      fs = FSS())
  }

  "Copy" should "show the usage message if the parameter number is incorrect" in {
    val ts = TerminalState(
      io = IOState(Nil, Nil),
      fs = FSS())

    terminal.cp(Seq.empty).exec(ts) shouldBe TerminalState(
      io = IOState(Nil, "Usage: cp <orig> <dest>\n" :: Nil),
      fs = FSS())
  }

  it should "copy a file" in {
    val ts = TerminalState(
      io = IOState(Nil, Nil),
      fs = FSS(
        "file1" -> "content1"))

    terminal.cp("file1" :: "file2" :: Nil).exec(ts) shouldBe TerminalState(
      io = IOState(Nil, Nil),
      fs = FSS(
        "file1" -> "content1",
        "file2" -> "content1"))
  }

  "Move" should "show the usage message if the parameter number is incorrect" in {
    val ts = TerminalState(
      io = IOState(Nil, Nil),
      fs = FSS())

    terminal.mv(Seq.empty).exec(ts) shouldBe TerminalState(
      io = IOState(Nil, "Usage: mv <orig> <dest>\n" :: Nil),
      fs = FSS())
  }

  it should "move a file" in {
    val ts = TerminalState(
      io = IOState(Nil, Nil),
      fs = FSS(
        "file1" -> "content1"))

    terminal.mv("file1" :: "file2" :: Nil).exec(ts) shouldBe TerminalState(
      io = IOState(Nil, Nil),
      fs = FSS(
        "file2" -> "content1"))
  }

  "Remove" should "show the usage message if the parameter number is incorrect" in {
    val ts = TerminalState(
      io = IOState(Nil, Nil),
      fs = FSS())

    terminal.rm(Seq.empty).exec(ts) shouldBe TerminalState(
      io = IOState(Nil, "Usage: rm <file-name>\n" :: Nil),
      fs = FSS())
  }

  it should "delete a file" in {
    val ts = TerminalState(
      io = IOState(Nil, Nil),
      fs = FSS(
        "file1" -> "content1"))

    terminal.rm("file1" :: Nil).exec(ts) shouldBe TerminalState(
      io = IOState(Nil, Nil),
      fs = FSS())
  }

  "Touch" should "show the usage message if the parameter number is incorrect" in {
    val ts = TerminalState(
      io = IOState(Nil, Nil),
      fs = FSS())

    terminal.touch(Seq.empty).exec(ts) shouldBe TerminalState(
      io = IOState(Nil, "Usage: touch <file-name>\n" :: Nil),
      fs = FSS())
  }

  it should "create an empty file" in {
    val ts = TerminalState(
      io = IOState(Nil, Nil),
      fs = FSS())

    terminal.touch("file1" :: Nil).exec(ts) shouldBe TerminalState(
      io = IOState(Nil, Nil),
      fs = FSS(
        "file1" -> ""))
  }

  "Touch2" should "show the usage message if the parameter number is incorrect" in {
    val ts = TerminalState(
      io = IOState(Nil, Nil),
      fs = FSS())

    terminal.touch2(Seq.empty).exec(ts) shouldBe TerminalState(
      io = IOState(Nil, "Usage: touch2 <file-name> [content]\n" :: Nil),
      fs = FSS())
  }


  it should "behave as `touch` if no content is provided" in {
    val ts = TerminalState(
      io = IOState(Nil, Nil),
      fs = FSS())

    terminal.touch2("file1" :: Nil).exec(ts) shouldBe TerminalState(
      io = IOState(Nil, Nil),
      fs = FSS(
        "file1" -> ""))
  }


  it should "create a file with content" in {
    val ts = TerminalState(
      io = IOState(Nil, Nil),
      fs = FSS())

    terminal.touch2("file1" :: "content1" :: "content2" :: Nil).exec(ts) shouldBe TerminalState(
      io = IOState(Nil, Nil),
      fs = FSS(
        "file1" -> "content1 content2"))
  }

  "Cat" should "show the usage message if the parameter number is incorrect" in {
    val ts = TerminalState(
      io = IOState(Nil, Nil),
      fs = FSS())

    terminal.cat(Seq.empty).exec(ts) shouldBe TerminalState(
      io = IOState(Nil, "Usage: cat <file-name>\n" :: Nil),
      fs = FSS())
  }

  it should "show file's content" in {
    val ts = TerminalState(
      io = IOState(Nil, Nil),
      fs = FSS(
        "file1" -> "content1"))

    terminal.cat("file1" :: Nil).exec(ts) shouldBe TerminalState(
      io = IOState(Nil, "content1" :: Nil),
      fs = FSS(
        "file1" -> "content1"))
  }

  "Echo" should "show the usage message if the parameter number is incorrect" in {
    val ts = TerminalState(
      io = IOState(Nil, Nil),
      fs = FSS())

    terminal.echo(Seq.empty).exec(ts) shouldBe TerminalState(
      io = IOState(Nil, "Usage: echo [content]\n" :: Nil),
      fs = FSS())
  }

  it should "echo the input" in {
    val ts = TerminalState(
      io = IOState(Nil, Nil),
      fs = FSS())

    terminal.echo("this is the input" :: Nil).exec(ts) shouldBe TerminalState(
      io = IOState(Nil, "this is the input\n" :: Nil),
      fs = FSS())
  }

  "Exit" should "exit from terminal" in {
    val ts = TerminalState(
      io = IOState(Nil, Nil),
      fs = FSS())

    terminal.exit.exec(ts) shouldBe TerminalState(
      io = IOState(Nil, "Good bye!\n" :: Nil),
      fs = FSS())
  }

  "Other" should "show an unknown command message" in {
    val ts = TerminalState(
      io = IOState(Nil, Nil),
      fs = FSS())

    terminal.other("?").exec(ts) shouldBe TerminalState(
      io = IOState(Nil, "Command not found: '?'\n" :: Nil),
      fs = FSS())
  }

  "Process commands" should "run read commands" in {
    val ts = TerminalState(
      io = IOState("echo hi, friend" :: "exit" :: Nil, Nil),
      fs = FSS())

    terminal.processCommands.exec(ts) shouldBe TerminalState(
      io = IOState(Nil, List(
        "Good bye!\n",
        "$ ",
        "hi, friend\n",
        "$ ")),
      fs = FSS())
  }

}
