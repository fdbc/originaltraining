package org.hablapps.fpinscala
package languages
package templates

import scala.io.StdIn.readLine
import org.scalatest._

object FunctionalAPIs extends FunctionalAPIs

class FunctionalAPIs extends FlatSpec with Matchers{

  /**
   * IO APIs
   */

  // trait IO{
  //   def read(): Future[String]
  //   def write(msg: String): Future[Unit]
  // }

  /**
   * API instance (asynchronous)
   */

  // import scala.concurrent.{Await, Future, ExecutionContext, duration}
  // import ExecutionContext.Implicits.global, duration._

  // object AsyncIO extends IO{
  //   def read(): Future[String] = ??? // Whatever
  //   def write(msg: String): Future[Unit] = ??? // Whatever
  // }

  /**
   * API instance (console-based)
   */

  // object ConsoleIO extends IO{
  //   def read() = readLine
  //   def write(msg: String) = println(msg)
  // }

  /**
   * API program
   */

  // def echo()(io: IO): String = {
  //   val msg: String = io.read()
  //   io.write(msg)
  //   msg
  // }

  /**
   * Composition
   */

  // def consoleEcho(): String =
  //   echo()(ConsoleIO)

}