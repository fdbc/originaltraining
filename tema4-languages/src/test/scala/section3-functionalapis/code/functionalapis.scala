package org.hablapps.fpinscala
package languages

import scala.io.StdIn.readLine
import org.scalatest._

object FunctionalAPIs extends FunctionalAPIs

class FunctionalAPIs extends FlatSpec with Matchers{

  // IO algebras

  trait IO[P[_]]{
    def read(): P[String]
    def write(msg: String): P[Unit]

    // Imperative combinators
    def returns[A](a: A): P[A]
    def doAndThen[A,B](p: P[A])(f: A => P[B]): P[B]
  }

  // Particular Monadic IO Program

  def echo[P[_]]()(io: IO[P]): P[String] =
    io.doAndThen(io.read()){ msg =>
      io.doAndThen(io.write(msg)){ _ =>
        io.returns(msg)
      }
    }

  // Console-based interpretation of IO Programs

  object Console{
    implicit object ConsoleIO extends IO[Id]{
      def read() = readLine()
      def write(msg: String) = println(msg)
      def returns[A](a: A): A = a
      def doAndThen[A,B](a: A)(f: A => B) = f(a)
    }
  }

  // asynchronous interpretation of IO Programs

  object Asynch{

    import scala.concurrent.{Await, Future, ExecutionContext, duration}
    import ExecutionContext.Implicits.global, duration._

    implicit object AsyncIO extends IO[Future]{
      def read() = ??? // Whatever
      def write(msg: String) = ??? // Whatever
      def returns[A](a: A): Future[A] =
        Future.successful(a)
      def doAndThen[A,B](a: Future[A])(f: A => Future[B]): Future[B] =
        a flatMap f
    }
  }

  // Echo interpretation (Console)

  def consoleEcho(): String =
    echo()(Console.ConsoleIO)

  // Echo interpretation (State transformations)

  case class IOState(reads: List[String], writes: List[String])

  type IOAction[T] = IOState => (IOState,T)

  object IOActionIO extends IO[IOAction]{
    def read(): IOAction[String] = {
      case IOState(msg::readsTail, writes) =>
        (IOState(readsTail, writes), msg)
      case _ => throw new Exception("not enough data to be read")
    }
    def write(msg: String): IOAction[Unit] = {
      case IOState(reads, writes) =>
        (IOState(reads, msg::writes), ())
    }
    def doAndThen[A,B](p: IOAction[A])(f: A => IOAction[B]): IOAction[B] =
      iostate1 => p(iostate1) match {
        case (iostate2, a) => f(a)(iostate2)
      }
    def returns[A](a: A): IOAction[A] =
      iostate => (iostate, a)
  }

  "State-based echo" should "work" in {

    echo()(IOActionIO)(IOState(List("hi"),List())) shouldBe
      (IOState(List(),List("hi")), "hi")

  }

}