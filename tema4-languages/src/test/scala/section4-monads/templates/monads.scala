package org.hablapps.fpinscala
package languages
package templates

import scala.io.StdIn.readLine
import org.scalatest._

object MonadicPrograms extends MonadicPrograms

class MonadicPrograms extends FlatSpec with Matchers{

  // IO API

  // trait IO[P[_]]{
  //   def read(): P[String]
  //   def write(msg: String): P[Unit]

  //   // Imperative combinators
  //   def returns[A](a: A): P[A]
  //   def doAndThen[A,B](p: P[A])(f: A => P[B]): P[B]
  // }

  // API program

  // def echo[P[_]]()(io: IO[P]): P[String] =
  //   io.doAndThen(io.read()){ msg =>
  //     io.doAndThen(io.write(msg)){ _ =>
  //       io.returns(msg)
  //     }
  //   }

  // API instances

  // type Id[T] = T

  // object Console{
  //   implicit object ConsoleIO extends IO[Id]{
  //     def read() = readLine()
  //     def write(msg: String) = println(msg)
  //     def returns[A](a: A): A = a
  //     def doAndThen[A,B](a: A)(f: A => B) = f(a)
  //   }
  // }

  // Composition

  // def consoleEcho(): String =
  //   echo()(Console.ConsoleIO)

}