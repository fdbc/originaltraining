package org.hablapps.fpinscala.languages
package test

import org.scalatest._

import scalaz.State

// IO Utils
case class IOState(reads: List[String], writes: List[String])

object IOState {

  type IOTrans[A] = State[IOState, A]

  import MonadicPrograms.IO
  implicit object IOTrans extends IO[IOTrans] {

    def read(): IOTrans[String] =
      for {
        s <- State.get
        _ <- State.put(s.copy(reads = s.reads.tail))
      } yield s.reads.head

    def write(msg: String): IOTrans[Unit] =
      State.modify{ s =>
        s.copy(writes = msg :: s.writes)
      }
      // Equivalently:
      // for {
      //   s <- State.get
      //   _ <- State.put(s.copy(writes = msg :: s.writes))
      // } yield ()
  }

  import MonadicPrograms.Monad
  implicit object MonadIOTrans extends Monad[IOTrans]{
    def doAndThen[A, B](f: IOTrans[A])(cont: A => IOTrans[B]): IOTrans[B] =
      f.flatMap(cont)

    def returns[A](a: A): IOTrans[A] =
      State.state(a)
  }

}
