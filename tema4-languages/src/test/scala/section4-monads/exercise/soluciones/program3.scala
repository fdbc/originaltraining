package org.hablapps.fpinscala
package languages
package solution

/**
 * Write a program that reads lines continously until it's read
 * the word "exit"
 *
 * For instance:
 * scala> readUntilExit
 *   <<type "hi" and enter>>
 *   <<type "bye" and enter>>
 *   <<type "exit" and enter>>
 */
object Program3{

  // Impure version
  import ConventionalApproach.Modular.IO

  trait Conventional{ IO: IO with Program1.Conventional =>
    import IO._

    def readUntilExit: Unit = {
      val msg = read
      if (msg == "exit") ()
      else readUntilExit
    }

  }

  // Declarative version
  object Declarative{
    import MonadicPrograms.{Monad, IO}, IO.Syntax._, Monad.Syntax._

    def readUntilExit[F[_]: IO: Monad]: F[Unit] =
      for {
        msg <- read
        _ <-  if (msg == "exit") returns(())
              else readUntilExit[F]
      } yield ()
  }

}
