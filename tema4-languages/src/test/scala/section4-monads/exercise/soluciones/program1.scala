package org.hablapps.fpinscala
package languages
package solution

/**
 * Write a program that reads from  standard input and says whether it's
 * even or ordd.
 *
 * For instance:
 *
 * scala> runWriteANumber
 * << type "8" >>
 * 8 is even
 *
 * scala> runWriteANumber
 * << type "5" >>
 * 5 is odd
 */
object Program1{

  // Helper method
  def evenOdd(n: String): String =
    if (n.toInt % 2 == 0)
      s"$n es un número par"
    else
      s"$n es un número impar"


  // Impure version, to be used as a specification
  import ConventionalApproach.Modular.IO

  trait Conventional{ IO: IO =>
    import IO._

    def writeANumber: Unit = {
      val num = read
      write(evenOdd(num))
    }

  }

  // Declarative version, to be implemented
  object Declarative{
    import MonadicPrograms.{Monad, IO}, IO.Syntax._, Monad.Syntax._

    def writeANumber[F[_]: IO: Monad]: F[Unit] =
      for {
        num <- read()
        _ <- write(evenOdd(num))
      } yield ()
  }

}
