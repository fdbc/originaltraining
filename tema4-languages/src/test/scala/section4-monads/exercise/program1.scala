package org.hablapps.fpinscala
package languages


/**
 * Write a program that reads from  standard input and says whether it's
 * even or ordd.
 *
 * For instance:
 *
 * scala> runWriteANumber
 * << type "8" >>
 * 8 is even
 *
 * scala> runWriteANumber
 * << type "5" >>
 * 5 is odd
 *
 * Use the following alias to test this exercise:
 *   `test-languages-exercise1`
 */
object Program1{

  // Helper method
  def evenOdd(n: String): String =
    if (n.toInt % 2 == 0)
      s"$n es un número par"
    else
      s"$n es un número impar"


  // Impure version, to be used as a specification

  object Conventional{
    import ConventionalApproach.Modular.IO

    def writeANumber()(IO: IO): Unit = {
      val num = IO.read()
      IO.write(evenOdd(num))
    }

  }

  // Declarative version, to be implemented

  object Declarative{
    import MonadicPrograms.{Monad, IO}, IO.Syntax._, Monad.Syntax._

    def writeANumber[F[_]: IO: Monad](): F[Unit] = ???
  }

}
