package org.hablapps.fpinscala
package languages

/**
 * Write a program that reads lines continously until it's read
 * the word "exit"
 *
 * For instance:
 * scala> readUntilExit
 *   <<type "hi" and enter>>
 *   <<type "bye" and enter>>
 *   <<type "exit" and enter>>
 *
 * Use the following alias to test this exercise:
 *   `test-languages-exercise1`
 */
object Program3{

  // Impure version

  object Conventional{
    import ConventionalApproach.Modular.IO

    def readUntilExit()(io: IO): Unit = {
      val msg = io.read()
      if (msg == "exit") ()
      else readUntilExit()(io)
    }

  }

  // Declarative version
  object Declarative{
    import MonadicPrograms.{Monad, IO}, IO.Syntax._, Monad.Syntax._

    def readUntilExit[F[_]: IO: Monad](): F[Unit] = ???
  }

}
