package org.hablapps.fpinscala
package languages

/**
 * Same as in `Program1` but now the program asks politely
 * for a number to the user.
 *
 * For instance:
 *
 *   scala> runWriteANumber2
 *   Please, type a number:
 *   << type 8 >>
 *   8 is even
 *
 * Use the following alias to test this exercise:
 *   `test-languages-exercise1`
 */
object Program2{

  // Impure version

  object Conventional{
    import ConventionalApproach.Modular.IO
    import Program1.Conventional._

    def writeANumberBis()(io: IO): Unit = {
      io.write("Introduce un número por favor:")
      writeANumber()(io)
    }

  }

  // Declarative version
  object Declarative{
    import MonadicPrograms.{Monad, IO}, IO.Syntax._, Monad.Syntax._
    import Program1.Declarative.writeANumber

    def writeANumberBis[F[_]: IO: Monad](): F[Unit] = ???
  }

}
