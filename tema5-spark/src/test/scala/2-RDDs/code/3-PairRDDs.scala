package org.hablapps.fpinscala.spark

import org.scalatest._

import org.apache.spark.rdd.RDD

object PairRDDs extends PairRDDs

class PairRDDs extends FunSpec with Matchers with SparkSetUpAndStop {

  lazy val bills: RDD[(String, Int)] =
    sc.parallelize(List(
      ("Luis",  5),
      ("Pepe", 20),
      ("Jose", 30),
      ("Pepe", 10),
      ("Jose",  5)))

  /**
   * Parte I
   * Acciones sobre pares de RDDs (PairRDDsFunctions)
   */

  describe("Inspecting Pair RDDs"){

    it("should work"){
      // cuenta de valores por clave

      lazy val billsPerUser: scala.collection.Map[String, Long] =
        bills countByKey()

      billsPerUser shouldBe Map("Luis" -> 1, "Pepe" -> 2, "Jose" -> 2)

      // retorna los valores para una clave dada

      lazy val joseBillsValues: Seq[Int] =
        bills lookup "Jose"

      joseBillsValues shouldBe List(30,5)
    }
  }

  /**
   * Parte II
   * Transformaciones sobre pares de RDDs (PairRDDFunctions)
   */
  describe("Pair transformations"){
    it("should work"){

      // transformación de valores

      lazy val amountsWithVAT: RDD[(String, Double)] =
        bills mapValues (_ * 1.2) // Asumiendo IVA 20%

      amountsWithVAT.collect shouldBe
        Array(("Luis",6.0), ("Pepe",24.0), ("Jose",36.0), ("Pepe",12.0), ("Jose",6.0))

      // agrupación de valores por clave

      lazy val amountsPerUser: RDD[(String, Iterable[Int])] =
        bills groupByKey()

      amountsPerUser lookup "Pepe" shouldBe
        Array(Array(20, 10))

      // combinación de valores por clave, sin valor zero (neutro). Obliga a que el tipo retornado sea el de los valores del RDD

      lazy val fullAmountPerUser: RDD[(String, Int)] =
        bills.reduceByKey(_ + _)

      fullAmountPerUser lookup "Pepe" shouldBe Array(30)

      // Esta vez de manera segura

      lazy val fullAmountPerUserSafer: RDD[(String, Int)] =
        bills.foldByKey(0)(_ + _)

      fullAmountPerUserSafer.collect.toMap.apply("Pepe") shouldBe 30

    }
  }

  /**
   * Parte III
   * Transformaciones sobre dos pares de RDDs (PairRDDFunctions),
   * son operaciones de conjuntos
   */
  lazy val addresses: RDD[(String, String)] =
    sc.parallelize(("Luis", "Avd Alamo 5") :: ("Jose", "Calle Pez 12") :: Nil)

  describe("Two pair transforms."){
    it("should work"){
      // sustraer por clave
      lazy val usersWithNoAddress: RDD[(String, Int)] =
        bills subtractByKey addresses

      usersWithNoAddress.map(_._1).distinct.collect shouldBe
        List("Pepe")

      // cogroup, aúna todos los valores por clave en ambos RDDs.
      // Si una clave no está en uno de los RDDs el Iterable correspondiente estará vacío
      lazy val usersBillsAndAddress: RDD[(String,(Iterable[Int], Iterable[String]))] =
        bills cogroup addresses

      usersBillsAndAddress lookup "Jose" shouldBe
        Array((List(30,5), List("Calle Pez 12")))

      // (inner) join, devuelve las keys que están en ambos RDDs, con sus valores. Si una clave está
      // repetida en alguno de los RDDs se hace producto cartesiano sobre los valores en ambos RDDs
      lazy val billsWithAddress: RDD[(String, (Int, String))] =
        bills join addresses

      billsWithAddress lookup "Jose" shouldBe
        Array((30,"Calle Pez 12"), (5, "Calle Pez 12"))
    }
  }


}
