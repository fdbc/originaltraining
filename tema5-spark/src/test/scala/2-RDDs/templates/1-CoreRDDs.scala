package org.hablapps.fpinscala.spark
package section2
package templates

import org.scalatest._

import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel

object CoreRDDs extends CoreRDDs

class CoreRDDs extends FunSpec with Matchers with SparkSetUpAndStop{

  /*
   * TRANSFORMACIONES
   */

  // Leemos las lineas de un archivo de texto
  lazy val lines: RDD[String] = ???

  // Separamos las líneas por palabras
  lazy val words: RDD[String] = ???

  // Filtramos las palabras vacías
  lazy val nonEmptyWords: RDD[String] = ???

  // Aplicamos una transformación para quedarnos únicamente con la longitud de las palabras
  lazy val wordsLength: RDD[Int] = ???

  /*
   * ACCIONES
   */
  describe("Actions"){
    ignore("should work"){

      // ¿Cuántas palabras hay?

      val wordsNumber: Long = ???

      wordsNumber shouldBe 59510

      // Palabras

      val wordsMat: Array[String] = ???

      wordsMat.slice(0,2) shouldBe Array("EL", "INGENIOSO")

      // ¿Cuáles son sus tamaños?

      val wordsLengthMat: Array[Int] = ???

      wordsLengthMat.slice(0,5) shouldBe Array(2,9,7,3,7)

      // ¿Cuáles son las longitudes de las 5 primeras palabras?

      val firstFiveWords: Array[Int] = ???

      firstFiveWords shouldBe Array(2,9,7,3,7)

      // ¿Y de la primera?

      val firstWord: Int = ???

      firstWord shouldBe 2

      // ¿Está vacío el conjunto?

      val isEmpty: Boolean = ???

      isEmpty shouldBe false

    }
  }

  /*
   * TRANSFORMACIONES SOBRE CONJUNTOS
   */

  // Ahora vamos a ver algunas transformaciones con conjuntos

  lazy val dice: RDD[Int] = sc.parallelize(1 to 6)

  lazy val oddDice: RDD[Int] = ???

  // Subtract

  lazy val evenDice: RDD[Int] = ???

  // Intersection

  lazy val emptyDS: RDD[Int] = ???

  // Union

  lazy val wholeDice: RDD[Int] = ???

  describe("Actions of set transformed RDDs"){
    ignore("should work"){

      dice.collect shouldBe ???

      oddDice.collect shouldBe ???

      evenDice.collect shouldBe ???

      emptyDS.collect shouldBe ???

      wholeDice.collect shouldBe ???
    }
  }

  /*
   * AGGREGATE, DE LAS TRANSFORMACIONES MÁS POTENTES
   */

  lazy val randomNumbers: RDD[Double] = sc.parallelize(1 to 1000).map(_ => math.random * 1000)

  describe("Aggregate actions"){

    ignore("like aggregate should work"){
      lazy val LengthAndSum: (Int,Double) =
        ???

      LengthAndSum._1 shouldBe 1000
    }

    ignore("like reduce and fold should work"){
      // ¿Cuantas letras hay en total?

      val charsNumber: Int = ???

      charsNumber shouldBe 251601

      // De una manera más segura

      val charsNumberSafer: Int = ???

      charsNumberSafer shouldBe 251601
    }

  }

  /*
   * GROUPBY Y SORTBY
   */

  describe("GroupBy y SortBy transformations"){

    // sample dataset
    lazy val simpleCol: RDD[Int] = sc.parallelize(Array(22, 42, 31, 41, 1, 6))

    ignore("should work"){
      val sortedDS: RDD[Int] =
        ???

      sortedDS.collect shouldBe ???

      val byEndings: RDD[(Int, Iterable[Int])] =
        ???

      byEndings.collect.toMap.apply(1) shouldBe
        ???
    }
  }

}
