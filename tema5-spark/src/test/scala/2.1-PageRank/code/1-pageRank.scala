package org.hablapps.fpinscala.spark

object RemovingVarsFromRDDs{

  import org.apache.spark.rdd.RDD

  // PageRank algorithm implemented with 'vars'

  object WithPlainVars{

    def pageRank(graph: RDD[(String,Seq[String])]): RDD[(String,Double)] = {
      var ranks: RDD[(String,Double)] = graph.mapValues(_ => 1.0)

      for (i <- 0 until 10){
        val contributions: RDD[(String, Double)] =
          graph.join(ranks).flatMap{
            case (node, (links,rank)) =>
              links.map(dest => (dest, rank/links.size))
          }.reduceByKey(_ + _)
        ranks = ranks.leftOuterJoin(contributions)
                  .mapValues{ case (_,mayBeCont) =>
                    0.15 + 0.85*mayBeCont.fold(0.0)(identity)
                  }
      }

      ranks
    }
  }

  object WithoutVarsTailRecursive{
    def pageRank(graph: RDD[(String,Seq[String])]): RDD[(String,Double)] = {
      @scala.annotation.tailrec
      def aux(i: Int, ranks: RDD[(String,Double)]): RDD[(String,Double)] =
        if (i==0) ranks
        else {
          val contributions: RDD[(String, Double)] =
            graph.join(ranks).flatMap{
              case (_, (links,rank)) =>
                links.map(dest => (dest, rank/links.size))
            }.reduceByKey(_ + _)
          aux(i-1,ranks.leftOuterJoin(contributions)
            .mapValues{ case (v,mayBeCont) =>
              0.15 + 0.85*mayBeCont.fold(0.0)(identity)
            })
        }

      aux(10,graph.mapValues(_ => 1.0))
    }
  }

  object WithCustomIterate{

    def iterate[T](n: Int)(initial: T)(step: T => T): T = {
      def aux(n: Int, result: T): T =
        if (n==0) result
        else aux(n-1,step(result))
      aux(n,initial)
    }

    def pageRank(graph: RDD[(String,List[String])]): RDD[(String,Double)] =
      iterate(10)(graph.mapValues(_ => 1.0)){ ranks =>
        val contributions: RDD[(String, Double)] =
          graph.join(ranks).flatMap{
            case (node, (links,rank)) =>
              links.map(dest => (dest, rank/links.size))
          }.reduceByKey(_ + _)
        ranks.leftOuterJoin(contributions)
          .mapValues{ case (_,mayBeCont) =>
            0.15 + 0.85*mayBeCont.fold(0.0)(identity)
          }
      }
  }

  object UsingScalaIterator{

    implicit class IteratorOps[A](it: Iterator[A]){
      @scala.annotation.tailrec
      final def nth(n: Int): Option[A] =
        if (!it.hasNext) None
        else if (n==0) Some(it.next)
        else { it.next; it.nth(n-1) }
    }

    def pageRank(graph: RDD[(String,List[String])]): RDD[(String,Double)] =
      Iterator.iterate(graph.mapValues(_ => 1.0)){ ranks =>
        val contributions: RDD[(String, Double)] =
          graph.join(ranks).flatMap{
            case (node, (links,rank)) =>
              links.map(dest => (dest, rank/links.size))
          }.reduceByKey(_ + _)

        ranks.leftOuterJoin(contributions)
          .mapValues{ case (_,mayBeCont) =>
            0.15 + 0.85*mayBeCont.fold(0.0)(identity)
          }
      }.nth(10).get
  }

}