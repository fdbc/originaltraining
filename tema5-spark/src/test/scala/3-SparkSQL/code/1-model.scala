package org.hablapps.fpinscala.spark
package sql

case class Client(id: Int, name: String, age: Int)

case class Transfer(orig: Int, dest: Int)
