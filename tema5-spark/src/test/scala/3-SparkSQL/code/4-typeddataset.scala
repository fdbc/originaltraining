package org.hablapps.fpinscala.spark
package sql

import org.apache.spark.SparkConf
import org.apache.spark.sql.{Dataset, SparkSession}
import frameless.{TypedColumn, TypedDataset}
import frameless.syntax._
import frameless.functions.aggregate.lit

object typeddataset {
  val conf = new SparkConf()
    .setMaster("local[*]")
    .setAppName("Spark SQL")

  val spark = SparkSession.builder()
    .config(conf)
    .getOrCreate()

  import spark.implicits._

  val cliTDS: TypedDataset[Client] = 
    spark.read.parquet("data/clients").as[Client].typed

  val youngClients1: TypedDataset[Int] = cliTDS
    .filter(cliTDS('age) < 30)
    .select(cliTDS('id))

  // This doesn't compile :)
  // val youngClients2: TypedDataset[Int] = cliTDS
  //   .filter(cliTDS('aged))
  //   .select(cliTDS('id))

  // This doesn't compile :)
  // val youngClients3: TypedDataset[String] = cliTDS
  //   .filter(cliTDS('age) < 30)
  //   .select(cliTDS('nick))

  // You can still use Dataset vanilla ops
  val youngClients4: TypedDataset[Int] = cliTDS
    .filter(_.age < 30)
    .map(_.id)

  // Casting
  case class ClientDOM(id: Long, name: String, age: Int)
  val domDS: TypedDataset[ClientDOM] =
    cliTDS.select(
      cliTDS('id).cast[Long],
      cliTDS('name),
      cliTDS('age)).as[ClientDOM]

  // Projection
  case class ShortClient(name: String, id: Int)
  val shortDS: TypedDataset[ShortClient] = cliTDS.project[ShortClient]

}
