package org.hablapps.fpinscala.spark
package dataframevalidation
package templates

import org.apache.spark.sql._, expressions._, types._, functions._

trait Validated[T]{

  // Spark Schema representing values of type `T`
  val Schema: StructType

  // ADT that represents the possible errors to be found
  // while analysing values of type T
  type Error
  val Error: Validated.ErrorCompanion[Error]

  // Validations for each field of type T. Keys of this map
  // must be members of `Error.Schema`
  val validations: Validated.Validations

  // Derived

  // Validates the specified data frame. It creates a new column
  // for each validation.
  def validate(df: DataFrame): DataFrame = ???

  // Filter valid rows of a validated data frame. It drops all validation
  // columns aftewards.
  def filterValid(df: DataFrame): DataFrame = ???

  // Filter invalid rows of a validated data frame that fail to pass
  // SOME of the specified validations. It drops any other validation column.
  def filterSomeInvalid(df: DataFrame)(invalidColumnNames: String*): DataFrame = ???

  // Filter rows of a validated data frame that fail to pass some validation
  def filterInvalid(df: DataFrame): DataFrame = ???

  // Filter invalid rows of a validated data frame that fail to pass
  // ALL of the specified validations. It drops any other validation column.
  def filterAllInvalid(df: DataFrame)(invalidColumnNames: String*): DataFrame = ???

}

object Validated{
  type Validations = Map[StructField, (StructField, UserDefinedFunction)]

  // Companion object of Error
  abstract class ErrorCompanion[Error]{

    // Spark schema representing values of type Error. Presence of
    // errors is signalled by non-null fields: there may be none, one or
    // several of them.
    val Schema: StructType
  }

  object Syntax{
    type Validation = (StructField, (StructField, UserDefinedFunction))

    def validation[T](errorField: String,
      valueField: String,
      udf: UserDefinedFunction)(implicit V: Validated[T]) =
      (V.Error.Schema(errorField), (V.Schema(valueField), udf))

    val init: Validations = Map()

    implicit class ValidationOps(validations: Validations){
      def +(v: Validation): Validations =
        validations + v
    }

    implicit class ValidatedOps(df: DataFrame){
      def validate[T](implicit V: Validated[T]) =
        V.validate(df)
      def filterValid[T](implicit V: Validated[T]) =
        V.filterValid(df)
      def filterInvalid[T](implicit V: Validated[T]) =
        V.filterInvalid(df)
      def filterAllInvalid[T](errorColumnNames: String*)(implicit V: Validated[T]) =
        V.filterAllInvalid(df)(errorColumnNames: _*)
      def filterSomeInvalid[T](errorColumnNames: String*)(implicit V: Validated[T]) =
        V.filterSomeInvalid(df)(errorColumnNames: _*)
    }
  }
}
