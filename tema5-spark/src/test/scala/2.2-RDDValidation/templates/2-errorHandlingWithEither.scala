package org.hablapps.fpinscala.spark
package templates

import org.scalatest._
import org.apache.spark.rdd.RDD
import com.holdenkarau.spark.testing.{RDDGenerator, SharedSparkContext}


object ErrorHandlingWithEither extends ErrorHandlingWithEither
class ErrorHandlingWithEither extends FunSpec with Matchers{

  import Domain._, DataSource._, Field._

  /* Auxiliary methods to improve type inference and readability */

  def wrong[T](e: Error): Either[Error,T] =
    Left(e)

  def right[T](v: T): Either[Error,T] =
    Right(v)

  /* Field validation and parsing */

  object FieldParsing{

    def parseName(field: Field): Either[Error, String] = ???

    import scala.util.Try

    def parseAge(field: Field): Either[Error, Int] = ???

    def parseGender(field: Field): Either[Error, Option[Boolean]] = ???
  }

  describe("Field validation"){
    import FieldParsing._

    ignore("should report errors for null values"){
      parseName(null) shouldBe ???
      parseAge(null) shouldBe ???
    }

    ignore("should report errors for wrong values"){
      parseAge("abc") shouldBe ???
      parseAge("-4") shouldBe ???
      parseGender("T") shouldBe ???
    }

    ignore("should return values for correct fields"){
      parseName("juan") shouldBe ???
      parseAge("4") shouldBe ???
      parseGender("H") shouldBe ???
      parseGender("V") shouldBe ???
      parseGender(null) shouldBe ???
      parseGender("") shouldBe ???
    }
  }

  /* Record validation and parsing */

  object RecordParsing{
    import FieldParsing._

    def parseRecord(record: Record): Either[Error, Person] = ???
  }

  describe("Record validation"){
    import RecordParsing._

    ignore("should report single errors for wrong records with a single error"){
      parseRecord(Array("Juan","-18","")) shouldBe ???
    }

    ignore("should report single errors for wrong records with several error"){
      parseRecord(Array("","-18","")) shouldBe ???
    }

    ignore("should parse correctly correct records"){
      parseRecord(Array("Juan","18","V")) shouldBe ???
      parseRecord(Array("Holden","30","")) shouldBe ???
      parseRecord(Array("Eva","48","H")) shouldBe ???
    }
  }
}

/* Dataset validation and parsing */

object DataSetParsing{
  import Domain._, DataSource._, Field._, ErrorHandlingWithEither._

  def parse(dataset: DataSet): RDD[Either[Error,Person]] = ???

  def parseRight(dataset: DataSet): RDD[Person] = ???

  def parseWrong(dataset: DataSet): RDD[(Record, Error)] = ???
}

class ErrorHandlingWithEitherDataSets extends FunSpec with Matchers with SharedSparkContext{ 
  
  describe("Dataset validation"){
    import Domain._, DataSource._, Field._, 
      ErrorHandlingWithEither.{right, wrong}, DataSetParsing._

    lazy val dataset = sc.parallelize(List(
        Array("Juan","18","V"),
        Array("Juan","-18",""),
        Array("Holden","30",""),
        Array("Eva","48","H")))

    it("should behave correctly for wrong and right"){
      parse(dataset).collect.toList shouldBe ???

      parseRight(dataset).collect.toList shouldBe ???

      parseWrong(dataset).map{ 
        case (key, value) => (key.toList,value)
      }.collectAsMap shouldBe ???
    }
  }
}

