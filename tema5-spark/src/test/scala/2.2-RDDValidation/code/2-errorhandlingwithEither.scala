package org.hablapps.fpinscala.spark

import org.scalatest._
import org.apache.spark.rdd.RDD
import com.holdenkarau.spark.testing.{RDDGenerator, SharedSparkContext}

object ErrorHandlingWithEither extends ErrorHandlingWithEither
class ErrorHandlingWithEither extends FunSpec with Matchers with SharedSparkContext{

  import Domain._, DataSource._, Field._

  /* Auxiliary methods to improve type inference and readability */

  def wrong[T](e: Error): Either[Error,T] =
    Left(e)

  def right[T](v: T): Either[Error,T] =
    Right(v)

  /* Field validation and parsing */

  object FieldParsing{

    def parseName(field: Field): Either[Error, String] =
      if (field==null) wrong(NotDefined(Field.Name))
      else if (field == "") wrong(WrongField(field, Field.Name))
      else right(field)

    import scala.util.Try

    def parseAge(field: Field): Either[Error, Int] =
      if (field==null) wrong(NotDefined(Field.Age))
      else Try(field.toInt).toOption
            .fold(wrong[Int](WrongField(field,Field.Age)))( int =>
              if (int < 0) wrong(NegativeAge(int))
              else right(int))


    def parseGender(field: Field): Either[Error, Option[Boolean]] =
      if (field==null) right(None)
      else if (field == "V") right(Some(true))
      else if (field == "H") right(Some(false))
      else if (field == "") right(None)
      else wrong(WrongField(field,Field.Gender))
  }

  describe("Field validation"){
    import FieldParsing._

    it("should report errors for null values"){
      parseName(null) shouldBe wrong(NotDefined(Name))
      parseAge(null) shouldBe wrong(NotDefined(Age))
    }

    it("should report errors for wrong values"){
      parseAge("abc") shouldBe wrong(WrongField("abc",Age))
      parseAge("-4") shouldBe wrong(NegativeAge(-4))
      parseGender("T") shouldBe wrong(WrongField("T",Gender))
    }

    it("should return values for correct fields"){
      parseName("juan") shouldBe right("juan")
      parseAge("4") shouldBe right(4)
      parseGender("H") shouldBe right(Some(false))
      parseGender("V") shouldBe right(Some(true))
      parseGender(null) shouldBe right(None)
      parseGender("") shouldBe right(None)
    }
  }

  /* Record validation and parsing */

  object RecordParsing{
    import FieldParsing._

    def parseRecord(record: Record): Either[Error, Person] = for{
      name <- parseName(record(Name.column)).right
      age <- parseAge(record(Age.column)).right
      gender <- parseGender(record(Gender.column)).right
    } yield Person(name,age,gender)
  }

  describe("Record validation"){
    import RecordParsing._

    it("should report single errors for wrong records with a single error"){
      parseRecord(Array("Juan","-18","")) shouldBe wrong(NegativeAge(-18))
    }

    it("should report single errors for wrong records with several error"){
      parseRecord(Array("","-18","")) shouldBe wrong(WrongField("",Field.Name))
    }

    it("should parse correctly correct records"){
      parseRecord(Array("Juan","18","V")) shouldBe right(Person("Juan",18,Some(true)))
      parseRecord(Array("Holden","30","")) shouldBe right(Person("Holden",30,None))
      parseRecord(Array("Eva","48","H")) shouldBe right(Person("Eva",48,Some(false)))
    }
  }
}

/* Dataset validation and parsing */

object DataSetParsing{
    import Domain._, DataSource._, Field._, ErrorHandlingWithEither._

    def parse(dataset: DataSet): RDD[Either[Error,Person]] =
      dataset map RecordParsing.parseRecord

    def parseRight(dataset: DataSet): RDD[Person] =
      parse(dataset) collect {
        case Right(person) => person
      }

    def parseWrong(dataset: DataSet): RDD[(Record, Error)] =
      dataset zip parse(dataset) collect {
        case (record,Left(error)) => (record, error)
      }
}

class ErrorHandlingWithEitherDataSets extends FunSpec with Matchers with SharedSparkContext{ 
  describe("Dataset validation"){
    import Domain._, DataSource._, Field._, ErrorHandlingWithEither.{right, wrong}, DataSetParsing._

    lazy val dataset = sc.parallelize(List(
        Array("Juan","18","V"),
        Array("Juan","-18",""),
        Array("Holden","30",""),
        Array("Eva","48","H")))

    it("should behave correctly for wrong and right"){
      parse(dataset).collect.toList shouldBe
        List(
          right(Person("Juan",18,Some(true))),
          wrong(NegativeAge(-18)),
          right(Person("Holden",30,None)),
          right(Person("Eva",48,Some(false))))

      parseRight(dataset).collect.toList shouldBe
        List(
          Person("Juan",18,Some(true)),
          Person("Holden",30,None),
          Person("Eva",48,Some(false)))

      parseWrong(dataset).map{ 
        case (key, value) => (key.toList,value)
      }.collectAsMap shouldBe(
        Map(List("Juan","-18","") -> NegativeAge(-18)))
    }
  }
}

