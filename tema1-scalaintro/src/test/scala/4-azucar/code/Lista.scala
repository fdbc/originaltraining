package org.hablapps.scalaintro.azucar
package code

// 0. Partimos de la lista del módulo 3, incluyendo los ejercicios en notación
// infija, y utilizamos azúcar sintáctico para las funciones.
object Step0{

  sealed trait Lista {
    def insertar(head: Int): Lista = new Cons(head, this)

    def suma: Int = this match {
      case Cons(cabeza, resto) => cabeza + resto.suma
      case Fin() => 0
    }

    def map(f: Int => Int): Lista = this match {
      case Cons(cabeza, resto) => Cons(f(cabeza), resto.map(f))
      case Fin() => Fin()
    }

    def concatenar(l: Lista): Lista = this match {
      case Cons(cabeza, resto) => Cons(cabeza, resto.concatenar(l))
      case Fin() => l
    }

    def existe(f: Int => Boolean): Boolean = this match {
      case Cons(cabeza, _) if f(cabeza) => true
      case Cons(_, resto) => resto.existe(f)
      case Fin() => false
    }

    def contiene(i: Int): Boolean = {
      existe(j => i == j)
    }

    def tirarMientras(f: Int => Boolean): Lista = this match {
      case Cons(cabeza, resto) if f(cabeza) => resto.tirarMientras(f)
      case _ => this
    }
  }

  case class Cons(head: Int, tail: Lista) extends Lista
  case class Fin() extends Lista
}

// 1. Renombramos métodos utilizando operadores simbólicos.
object Step1{
  sealed trait Lista {

    def insertar(head: Int): Lista = new Cons(head, this)

    def ::(head: Int): Lista = insertar(head)

    def suma: Int = this match {
      case Cons(cabeza, resto) => cabeza + resto.suma
      case Fin() => 0
    }

    def map(f: Int => Int): Lista = this match {
      case Cons(cabeza, resto) => Cons(f(cabeza), resto.map(f))
      case Fin() => Fin()
    }

    def concatenar(l: Lista): Lista = this match {
      case Cons(cabeza, resto) => Cons(cabeza, resto.++(l))
      case Fin() => l
    }

    def ++(l: Lista): Lista = concatenar(l)

    def existe(f: Int => Boolean): Boolean = this match {
      case Cons(cabeza, _) if f(cabeza) => true
      case Cons(_, resto) => resto.existe(f)
      case Fin() => false
    }

    def contiene(i: Int): Boolean = {
      existe(j => i == j)
    }

    def tirarMientras(f: Int => Boolean): Lista = this match {
      case Cons(cabeza, resto) if f(cabeza) => resto.tirarMientras(f)
      case _ => this
    }
  }

  case class Cons(head: Int, tail: Lista) extends Lista
  case class Fin() extends Lista
}

// 2. Aplicación de parámetros por defecto en el constructor de `Cons`.
object Step2{
  sealed trait Lista {
    def insertar(head: Int): Lista = new Cons(head, this)

    def ::(head: Int): Lista = insertar(head)

    def suma: Int = this match {
      case Cons(cabeza, resto) => cabeza + resto.suma
      case Fin() => 0
    }

    def map(f: Int => Int): Lista = this match {
      case Cons(cabeza, resto) => Cons(f(cabeza), resto.map(f))
      case Fin() => Fin()
    }

    def concatenar(l: Lista): Lista = this match {
      case Cons(cabeza, resto) => Cons(cabeza, resto.++(l))
      case Fin() => l
    }

    def ++(l: Lista): Lista = concatenar(l)

    def existe(f: Int => Boolean): Boolean = this match {
      case Cons(cabeza, _) if f(cabeza) => true
      case Cons(_, resto) => resto.existe(f)
      case Fin() => false
    }

    def contiene(i: Int): Boolean = {
      existe(j => i == j)
    }

    def tirarMientras(f: Int => Boolean): Lista = this match {
      case Cons(cabeza, resto) if f(cabeza) => resto.tirarMientras(f)
      case _ => this
    }
  }

  case class Cons(head: Int, tail: Lista = Fin()) extends Lista
  case class Fin() extends Lista
}

// 3. Método variadico `crear` para creación de listas.
object Step3{
  sealed trait Lista {
    def insertar(head: Int): Lista = new Cons(head, this)

    def ::(head: Int): Lista = insertar(head)

    def suma: Int = this match {
      case Cons(cabeza, resto) => cabeza + resto.suma
      case Fin() => 0
    }

    def map(f: Int => Int): Lista = this match {
      case Cons(cabeza, resto) => Cons(f(cabeza), resto.map(f))
      case Fin() => Fin()
    }

    def concatenar(l: Lista): Lista = this match {
      case Cons(cabeza, resto) => Cons(cabeza, resto.++(l))
      case Fin() => l
    }

    def ++(l: Lista): Lista = concatenar(l)

    def existe(f: Int => Boolean): Boolean = this match {
      case Cons(cabeza, _) if f(cabeza) => true
      case Cons(_, resto) => resto.existe(f)
      case Fin() => false
    }

    def contiene(i: Int): Boolean = {
      existe(j => i == j)
    }

    def tirarMientras(f: Int => Boolean): Lista = this match {
      case Cons(cabeza, resto) if f(cabeza) => resto.tirarMientras(f)
      case _ => this
    }
  }

  case class Cons(head: Int, tail: Lista) extends Lista
  case class Fin() extends Lista

  object Lista{
    def crear(elems: Int*): Lista =
      if (elems.isEmpty) Fin()
      else Cons(elems.head, Lista.crear(elems.tail: _*))
  }
}

// 4. Renombramiento de `crear` por `apply`

object Step4{
  sealed trait Lista {
    def insertar(head: Int): Lista = new Cons(head, this)

    def ::(head: Int): Lista = insertar(head)

    def suma: Int = this match {
      case Cons(cabeza, resto) => cabeza + resto.suma
      case Fin() => 0
    }

    def map(f: Int => Int): Lista = this match {
      case Cons(cabeza, resto) => Cons(f(cabeza), resto.map(f))
      case Fin() => Fin()
    }

    def concatenar(l: Lista): Lista = this match {
      case Cons(cabeza, resto) => Cons(cabeza, resto.++(l))
      case Fin() => l
    }

    def ++(l: Lista): Lista = concatenar(l)

    def existe(f: Int => Boolean): Boolean = this match {
      case Cons(cabeza, _) if f(cabeza) => true
      case Cons(_, resto) => resto.existe(f)
      case Fin() => false
    }

    def contiene(i: Int): Boolean = {
      existe(j => i == j)
    }

    def tirarMientras(f: Int => Boolean): Lista = this match {
      case Cons(cabeza, resto) if f(cabeza) => resto.tirarMientras(f)
      case _ => this
    }
  }

  case class Cons(head: Int, tail: Lista) extends Lista
  case class Fin() extends Lista

  object Lista{
    def apply(elems: Int*): Lista =
      if (elems.isEmpty) Fin()
      else Cons(elems.head, Lista(elems.tail: _*))
  }
}
