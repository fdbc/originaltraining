package org.hablapps.scalaintro.azucar
package templates

// 0. Partimos de la lista del módulo 3, incluyendo los ejercicios en notación
// infija.
object Step0{

  sealed trait Lista {
    def insertar(head: Int): Lista = new Cons(head, this)

    def suma: Int = this match {
      case Cons(cabeza, resto) => cabeza + resto.suma
      case Fin() => 0
    }

    def map(f: Function1[Int, Int]): Lista = this match {
      case Cons(cabeza, resto) => Cons(f(cabeza), resto.map(f))
      case Fin() => Fin()
    }

    def concatenar(l: Lista): Lista = this match {
      case Cons(cabeza, resto) => Cons(cabeza, resto.concatenar(l))
      case Fin() => l
    }

    def existe(f: Function1[Int, Boolean]): Boolean = this match {
      case Cons(cabeza, _) if f(cabeza) => true
      case Cons(_, resto) => resto.existe(f)
      case Fin() => false
    }

    def contiene(i: Int): Boolean = {
      existe(j => i == j)
    }

    def tirarMientras(f: Function1[Int, Boolean]): Lista = this match {
      case Cons(cabeza, resto) if f(cabeza) => resto.tirarMientras(f)
      case _ => this
    }
  }

  case class Cons(head: Int, tail: Lista) extends Lista
  case class Fin() extends Lista
}

// 1. Renombramos métodos utilizando operadores simbólicos.
object Step1{
}

// 2. Aplicación de parámetros por defecto en el constructor de `Cons`.
object Step2{
}

// 3. Método variadico `crear` para creación de listas.
object Step3{
}

// 4. Renombramiento de `crear` por `apply`

object Step4{
}
