package org.hablapps.scalaintro.funcional
package templates

// 1. Crear estructura propia para poder trabajar con listas.
object Step1{
}

// 2. Sellamos y cambiamos las clases por "case" clases
object Step2{
}

// 3. Añadimos un método para insertar un nuevo elemento en la lista (por la
// cabeza)
object Step3{
}

// 4. Pattern matching. Añadimos el método `suma`, que suma todos los elementos de la lista,
// o devuelve 0 en caso de que la lista sea vacía.
object Step4{
}

// 5. Functions. Añadimos el método `map` que recibe una función (Function1[Int,Int]) para mapear
// todos los elementos de esta lista.
object Step5{
}