package org.hablapps.scalaintro.oo
package templates

// 1. Creación de una bicicleta MUTABLE, con cadencia, marcha y velocidad. Los
// campos van definidos en el bloque, utilizando los argumentos del constructor.

object Step1{
}

// 2. Versión INMUTABLE de la bicicleta anterior. Utilizamos `val` (valor) en
// lugar de `var` (variable). Scala fomenta la inmutabilidad, por lo que el uso
// de `var`s está desaconsejado. Como consecuencia, desaparecen los setters.

object Step2{
}

// 3. Los constructores y los campos se pueden unificar. Si algún argumento
// prescinde del modificador `val` no se creará un campo para él en la clase. En
// tal caso, será considerado como un argumento de entrada del constructor sin
// más.

object Step3{
}

// 4. Podemos declarar constructores adicionales mediante `def this`:

object Step4{
}

// 5. Los métodos se añaden utilizando `def`. Podemos, por ejemplo, permitir que
// nuestra bicicleta pueda frenar la velocidad dado un decremento.

object Step5{
}


// 6. Es posible tener clases con una única instancia, lo que se conoce como
// Singleton Objects. Este componente nos permite reemplazar los miembros de
// clase propios de Java. Crearemos una `FabricaDeBicicletas` con valores
// iniciales y un método de creación de bicicletas `crear`.

object Step6{
  import Step5._
}


// 7. Los Companion Objects son singleton objects (o simplemente objects) que
// acompañan a un tipo de datos, por ejemplo a la clase bicicleta.

object Step7{
  import Step5._
}

// 8. Podemos aplicar herencia básica entre clases de forma muy sencilla.
// Declararemos una bicicleta de montaña que hereda de nuestra bicicleta,
// añadiendo un nuevo campo `alturaSillin`.
object Step8{
  import Step5._
}

// 9. Si una clase tiene algún miembro no definido tendrá que permanecer
// abstracta. Se declara una bicicleta de carretera que no implementa una altura
// para el sillín.
object Step9{
  import Step5._
}


// 10. Los traits son interfaces que permiten implementación parcial, con lo que
// se permite la herencia múltiple. Por ejemplo definiremos un motor con una
// cilindrada fija que también añade un campo revoluciones.
object Step10{
}

// 11. De esta forma podemos definir nuestra bicicleta con motor, extendiendo la
// clase Bicicleta y el trait Motor.
object Step11{
  import Step10._, Step5._
}