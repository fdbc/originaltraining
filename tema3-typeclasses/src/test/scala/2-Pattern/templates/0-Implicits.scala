package org.hablapps.fpinscala.typeclasses
package pattern
package templates

import org.scalatest._

class Implicits extends towards.code.AggregationProblem{

  trait Monoid[T]{
    val zero: T
    def combine(t1: T,t2: T): T
  }

  object Monoid{
    
  }

  object ImplicitParameters {

    // If we declared the monoid instance as "implicit", then we may
    // let the compiler in charge of looking for an instance of the
    // monoid.

    // def collapse[A](l: List[A])(ev: Monoid[A]): A = l match {
    //   case h :: t => ev.combine(h, collapse(t)(ev))
    //   case Nil => ev.zero
    // }

    // The compiler will look for implicit instances in the current scope,
    // but also in the companion objects of the type involved.

    // /*implicit*/ val intMonoid = Monoid.intMonoid
    // /*implicit*/ val stringMonoid = Monoid.stringMonoid

    // The dependency will be injected automatically by the compiler.
    // The compiler option "-Xprint:typer" can be used to know which exact
    // instance is passed by the compiler.

    def sum(l: List[Int]): Int = ???
      // collapse(l)(intMonoid)

    def concat(l: List[String]): String = ???
      // collapse(l)(stringMonoid)
  }

  ignore("La version con parámetros implicitos") {
    import ImplicitParameters._

    testAggregationInt(sum)
    testAggregationString(concat)
  }

  object ContextBounds {
    import ImplicitParameters._

    // Context bounds can be used as well, but now we need to summon the
    // type class instance with `implicitly[_]`, which is cumbersome.

    // def collapse[A](l: List[A])(ev: Monoid[A]): A = l match {
    //   case h :: t => ev.combine(h, collapse(t)(ev))
    //   case Nil => ev.zero
    // }

    def sum(l: List[Int]): Int = ???
      // collapse(l)(intMonoid)

    def concat(l: List[String]): String = ???
      // collapse(l)(stringMonoid)
  }

  ignore("La version con context bounds en parámetros genéricos") {
    import ContextBounds._

    testAggregationInt(sum)
    testAggregationString(concat)
  }

  object AvoidingImplicitly {
    import ImplicitParameters._

    // To avoid `implicitly`, we can use some syntactic sugar: helper
    // methods, possibly implemented with implicit classes for infix operators.

    object MonoidSyntax {

      // Binary ops

      // Non-binary ops
    }

    // We can now write our function more concisely.
    import MonoidSyntax._

    // def collapse[A](l: List[A])(ev: Monoid[A]): A = l match {
    //   case h :: t => ev.combine(h, collapse(t)(ev))
    //   case Nil => ev.zero
    // }

    def sum(l: List[Int]): Int = ???
      // collapse(l)(intMonoid)

    def concat(l: List[String]): String = ???
      // collapse(l)(stringMonoid)
  }

  ignore("La version con azúcar sintáctico") {
    import AvoidingImplicitly._

    testAggregationInt(sum)
    testAggregationString(concat)
  }
}












