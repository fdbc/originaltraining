package org.hablapps.fpinscala.typeclasses
package pattern
package code

import org.scalatest._

class MonoidSpec extends FunSpec with Matchers {

  describe("La typeclass Monoid") {
    import Monoid.syntax.{empty => zero, _}

    it("cumple las leyes de los monoides para Int") {

      zero[Int] |+| 5 shouldBe 5
      3 |+| zero[Int] shouldBe 3

      (3 |+| 7) |+| 1 shouldBe 3 |+| (7 |+| 1)
    }

    it("cumple las leyes de los monoides para String") {

      zero[String] |+| "5" shouldBe "5"
      "3" |+| zero[String] shouldBe "3"

      ("3" |+| "7") |+| "1" shouldBe "3" |+| ("7" |+| "1")
    }
  }

}
