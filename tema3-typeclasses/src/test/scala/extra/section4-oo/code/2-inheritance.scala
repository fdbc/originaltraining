package org.hablapps.fpinscala.typeclasses
package oo
package code

object Inheritance{

  /**
   * (II) Recurrrent pattern: failed attempt with inheritance
   */
  object RecurrentPattern{

    /**
     * Base class
     */
    trait Order[This <: Order[This]]{ self =>
      def compare(other: This): Int
      // derived
      def greaterThan(t2: This): Boolean = compare(t2) > 0
      def equalThan(t2: This): Boolean = compare(t2) == 0
      def lowerThan(t2: This): Boolean = compare(t2) < 0
    }

    /**
     * Generic implementation
     */
    def greatest[A <: Order[A]](l: List[A]): Option[A] =
      l.foldLeft(None: Option[A]){
        (acc, x) => acc.fold(Option(x)){
          y => if (x.lowerThan(y)) Option(y) else Option(x)
        }
      }

  }

  /**
   * (III) Modularised versions
   */
  object ModularPrograms{
    import RecurrentPattern._

    // Can't redefine standard types
    // e.g. `class Int extends Order ...`

    // We can only do it with our own classes

    case class Potato(color: (Int, Int, Int), size: Int) extends Order[Potato]{
      def compare(other: Potato): Int =
        this.size - other.size
    }

    // Modularised versions

    def greatestPotato(l: List[Potato]): Option[Potato] =
      greatest(l)
  }
}