package org.hablapps.fpinscala.typeclasses
package oo
package code

import org.scalatest._

class TypeClasses extends FunSpec with Matchers{

  /**
   * (II) Recurrent patterns
   */

  import support.code.Implicits.Monoid
  import support.code.Implicits.ImplicitParameters.collapse

    
  /**
   * (III) Modularisation
   */
  
  import intro.code.MonolythicPrograms.Potato

  implicit object potatoMonoid extends Monoid[Potato]{
    val zero = Potato((0,0,0),0)
    def add(t1: Potato, t2: Potato): Potato =
      Potato(t1.color, t1.size + t2.size)
  }

  def smashPotatos(l: List[Potato]): Potato =
    collapse[Potato](l)
}












