package org.hablapps.fpinscala.typeclasses
package intro
package code

import org.scalatest._

/**
 * (I) Monolythic versions
 */
object MonolythicPrograms extends MonolythicPrograms

class MonolythicPrograms extends FunSpec with Matchers with IntroSpec{

  /* For predefined types */

  def sum(l: List[Int]): Int =
    l match {
      case Nil => 0
      case x :: r => x + sum(r)
    }

  def concat(l: List[String]): String =
    l match {
      case Nil => ""
      case x :: r => x + concat(r)
    }

  /* For our own types */

  case class Potato(color: (Int, Int, Int), size: Int)
  
  def smashPotatos(l: List[Potato]): Potato =
    l match {
      case Nil => Potato((0,0,0),0)
      case x :: r => Potato(x.color, x.size + smashPotatos(r).size)
    }

  describe("Monolythic functions") {
    addIntTest(sum)
    concatTest(concat)
  }

}
