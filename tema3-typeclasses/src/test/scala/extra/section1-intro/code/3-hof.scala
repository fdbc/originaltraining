package org.hablapps.fpinscala.typeclasses
package intro
package code

import org.scalatest._

/**
  Parametric polymorphism is not enough sometimes, when we need special
  information about the type parameter. That extra information can be passed
  to the function as additional arguments (values or functions). 
 */
object HigherOrderFunctions extends HigherOrderFunctions

class HigherOrderFunctions extends FunSpec with Matchers with IntroSpec {

  // Abstraction

  def collapse[A](l: List[A])(
    zero: A, add: (A,A) => A): A =
    l match {
      case Nil => zero
      case x :: r => add(x,collapse(r)(zero,add))
    }

  describe("// Modularised function collapse") {
    addIntTest(collapse(_)(0, _ + _))
    concatTest(collapse(_)("", _ + _))

    it("serves for other purposes as well"){
      collapse(List(1, 2, 3, 4))(1, _ * _) shouldBe 24
    }
  }

  // Composition

  def sumBis(l: List[Int]): Int =
    collapse(l)(0, _+_)

  def concatBis(l: List[String]): String =
    collapse(l)("", _+_)

  describe("Modularised functions") {
    addIntTest(sumBis)
    concatTest(concatBis)
  }

}
