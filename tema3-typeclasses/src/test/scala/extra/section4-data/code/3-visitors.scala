package org.hablapps.fpinscala.typeclasses
package data
package code

object Visitors {

  // We can evaluate in an indirect way an arithmetic expression created
  // with the type class, by interpreting it as an `ADT.Expr`.
  object IndirectEvaluation{
    import Factories._

    // val v: Int = ADT.eval(expr(Exp.ADTExpInstance))
    val v: Int = ADT.eval(expr)
  }

  // But we can bypass `ADT.Expr` and evaluate it directly
  object DirectEvaluation{

    // Interpreter
    import Factories._

    implicit object eval extends Exp[Int] {
      def lit(i: Int): Int = i
      def add(e1: Int, e2: Int): Int = e1 + e2
    }

    // Interpretation
    import Factories._

    val v1: Int = expr(eval)

    // or directly
    import Exp.syntax._
    val v: Int = lit(1) + lit(3)
  }

}