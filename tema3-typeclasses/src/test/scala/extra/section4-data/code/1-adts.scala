package org.hablapps.fpinscala.typeclasses
package data
package code

object ADT {

  // Data type

  sealed trait Exp
  case class Lit(x: Int) extends Exp
  case class Add(l: Exp, r: Exp) extends Exp

  // Functionality

  def eval(e: Exp): Int = e match {
    case Lit(i) => i
    case Add(l, r) => eval(l) + eval(r)
  }

  // Execution

  val expr: ADT.Exp = ADT.Add(ADT.Lit(1), ADT.Lit(3))

  val v1: Int = ADT.eval(expr)

}

