package org.hablapps.fpinscala.typeclasses
package data
package code

object Factories {

  // Type class `Exp` represents the class of types `E` that 
  // behave as arithmetic expressions. It can be understood as 
  // an abstract factory of expressions.
  trait Exp[E] {
    // 1. Abstract
    def lit(i: Int): E
    def add(e1: E, e2: E): E

    // 2. Concrete
  }

  object Exp extends ExpInstances
    with ExpSyntax
    with ExpLaws

  // 3. Instances
  trait ExpInstances {
    def apply[E](implicit ev: Exp[E]) = ev

    // The ADT Exp is an instance of this type class
    implicit object ADTExpInstance extends Exp[ADT.Exp]{
      def lit(i: Int): ADT.Exp = ADT.Lit(i)
      def add(e1: ADT.Exp, e2: ADT.Exp): ADT.Exp = ADT.Add(e1,e2)
    }
  }

  // 4. Syntax
  trait ExpSyntax {
    object syntax{
      def lit[E](i: Int)(implicit E: Exp[E]) = E.lit(i)

      implicit class BinOps[E](e1: E)(implicit E: Exp[E]){
        def +(e2: E) = E.add(e1,e2)
      }
    }
  }

  // 5. Laws
  trait ExpLaws {}

  // Example of arithmetic expression defined in abstract terms
  import Exp.syntax._
  def expr[E: Exp]: E = lit(3) + lit(7)

  // Interpretation of expressions

  // Concrete expressions for ADT Exp
  val expr1: ADT.Exp = expr[ADT.Exp] // (Exp.ADTExpInstance)

}
