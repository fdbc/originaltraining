package org.hablapps.fpinscala.typeclasses
package data

import org.scalatest._

package object templates extends Templates

class Templates extends FunSpec with Matchers {

  describe("Evaluación indirecta"){
    import code.ADT

    it("Sin utilizar la sintaxis"){
      val expr1: ADT.Exp = ???
      expr1 shouldBe (??? : ADT.Exp)
    }

    it("Utilizando la sintaxis"){
      
    }
  }

  describe("Evaluación directa"){
    
    it("Sin utilizar la sintaxis"){
      val v: Int = ???
      v shouldBe (??? : Int)
    }

    it("Utilizando la sintaxis"){
      val v: Int = ???
      v shouldBe (??? : Int)
    }
  }

}
