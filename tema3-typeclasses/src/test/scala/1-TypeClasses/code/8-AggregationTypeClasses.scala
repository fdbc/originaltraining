package org.hablapps.fpinscala.typeclasses
package towards
package code

class AggregationMonoid extends AggregationProblem{

  // 3. Oops! With adapters we can't make use of static values! There's no way
  // we can implement this function with adapters. Gentlemen, I present you
  // the pattern of patterns... Typeclass!
  // The problem with adapters is that it's coupled/attached with a value,
  // so we can define members that are independent of the concrete instance.
  // Let's fix this

  trait Monoid[A] {
    def combine(a1: A, a2: A): A
    def zero: A
  }

  // 4. Now that we have decoupled the functionality from the value, we can
  // implement the `collapse` method without issues

  def collapse[A](l: List[A])(ev: Monoid[A]): A = l match {
    case h :: t => ev.combine(h, collapse(t)(ev))
    case Nil => ev.zero
  }

  // 5. As we did with adapters, now we have to give instances for the types
  // we are interested on. These instances are not going to be classes but
  // values/objects, because they are no longer tied with a value.

  val intMonoid = new Monoid[Int] {
    def combine(a1: Int, a2: Int) = a1 + a2
    def zero = 0
  }

  object potatoMonoid extends Monoid[PotatoImpl]{
    val zero = PotatoImpl((0,0,0),0)
    def combine(t1: PotatoImpl, t2: PotatoImpl): PotatoImpl =
      PotatoImpl(t1.color, t1.size + t2.size)
  }

  // Tests
  
  describe("Aggregation problem: with type classes"){
    testAggregationInt(collapse[Int](_)(intMonoid))
    testAggregationPotato(collapse[PotatoImpl](_)(potatoMonoid))
  }


}
