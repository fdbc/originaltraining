package org.hablapps.fpinscala

package object typeclasses {

  type Id[A] = A
  type ConstF[A, B] = A

  implicit class Implication(b1: Boolean){
    def ==>(b2: Boolean): Boolean = 
      !b1 || b2
  }
}